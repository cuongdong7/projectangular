var mysql = require('mysql');

function Connection() {
    var connect = mysql.createConnection({
            connectionLimit: 10,
             host: 'localhost',
             user: 'root',
             password: 'password',
             database: 'quanlybanhoa',
             multipleStatements : true
        });
    return connect;
}

module.exports = {
    Connection : Connection,
}