var app = angular.module('myApp', []);

app.controller('myController', myController);

function myController($scope, $http) {
  $scope.title = 'Danh Sách Flowers!!!';
  $scope.listFlowers = [];
  $scope.listTypeFlowers = [];

  $scope.getListFlowers = function(){
    $http.get('/flower').then(function(result) {
      $scope.listFlowers = result.data;
    });
  };
  $scope.getListFlowers();

  $scope.getListTypeFlowers = function(){
    $http.get('/typeflower').then(function(result) {
      $scope.listTypeFlowers = result.data;
    });
  };
  $scope.getListTypeFlowers();
}

